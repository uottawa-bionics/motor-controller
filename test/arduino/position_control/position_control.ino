// Position Control
// Joshua O'Reilly
// 2019/04/26
// This program uses a basic PID controller to achieve desired motor positions
// See attached images for wiring

// For this setup:
// Digital Output 1: PWM set value
//        10%: 0rpm
//        90%: 1500rpm
// Digital Output 2: Enable or disable
// Digital Output 3: Direction (high is CCW, low is CW)
// Digital Output 4: Stop
// Analog Input 0: Rotational Velocity 
const int pwm_pin = 5;
const int enable_pin = 2;
const int ccw_pin = 4;
const int stop_pin = 7;
const int current_speed_pin = A0;
const int position_pin = A5;

// Error and position tracking
float error_current = 0;
float error_previous = 0;
float position_current = 0;
int position_from_gyro = 0;

// Controller gains
// Decent gain settings with low overshoot seem to be k_p=1000, k_d=700,k_i=? 
const float k_p = 1000;
const float k_d = 0;
const float k_i = 0;
float PID_p = 0;
float PID_d = 0;
float PID_i = 0;
float PID_output = 0; // What comes out of the PD controller

// Other variables
float sensor_value_rpm = 0;
float delta_x = 0;
float time_current, time_previous, time_elapsed;

// Desired positions (in degrees)
float position_desired = 0;

void setup() {
  // set the digital pin as output and analog pin as input:
  pinMode(pwm_pin, OUTPUT);
  pinMode(enable_pin, OUTPUT);
  pinMode(ccw_pin, OUTPUT);
  pinMode(stop_pin, OUTPUT);
  pinMode(current_speed_pin, INPUT);
  pinMode(position_pin, INPUT);
  // Initialize serial communication at 9600bps (used to track velocity)
  Serial.begin(9600);
  time_current = millis();
}


// PID Controller inspired from:
// http://electronoobs.com/eng_arduino_tut24_2.php
void loop() {
  digitalWrite(stop_pin, LOW);
  digitalWrite(enable_pin, HIGH);

  position_from_gyro = analogRead(position_pin);
  Serial.println(position_current);
  position_current = float(position_from_gyro - 90);

  sensor_value_rpm = get_current_rpm();
  time_previous = time_current;
  time_current = millis();
  time_elapsed = (time_current - time_previous) / 1000;
  // Divide by 160 due to 160:1 gear ratio
  delta_x = (360.0 / 160.0) * (sensor_value_rpm / 60.0) * time_elapsed; // Lazy/poor quality integration approximation
  position_current += delta_x;
  error_current = position_desired - position_current;
  
  PID_p = k_p * error_current;
  PID_d = k_d * ((error_current - error_previous) / time_elapsed);
  PID_i = PID_i + (k_i * error_current);
  PID_output = PID_p + PID_d + PID_i;
  
  // Reverse the direction of rotation if needed
  if(PID_output > 0){
    digitalWrite(ccw_pin, HIGH);
  } else{
    digitalWrite(ccw_pin, LOW);
    PID_output = PID_output * -1;
  }
  if(PID_output > 230){
    PID_output = 230;
  }
  analogWrite(pwm_pin, PID_output);
  error_previous = error_current;
  delay(10);
}

/*
 * analogRead maps 0-5V between 0-1023
 * Since Arduino accepts 5V in, we need
 * 4/5 of max value be equal to 1600rpm (since we'll never read over 4/5 of 5V)
 * (This all assumes that the mappings made by ESCON and Arduino are linear)
 */
int get_current_rpm(){
  float sensor_value_raw = analogRead(current_speed_pin);
  // 818 is 4/5 of 1023
  float sensor_value_rpm = map(sensor_value_raw, 0, 818, -1610, 1610);
  return sensor_value_rpm;
}
