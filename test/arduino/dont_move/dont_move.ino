// Read RPM
// Joshua O'Reilly
// 2019/04/26
// This program sends out an analog signal and reads the rpm from the motor
// See attached images for wiring

// For this setup:
// Digital Output 1: PWM set value
//        10%: 0rpm
//        90%: 1500rpm
// Digital Output 2: Enable or disable
// Digital Output 3: Direction (high is CCW, low is CW)
// Digital Output 4: Stop
// Analog Input 0: Rotational Velocity 
const int pwm_pin = 5;
const int enable_pin = 2;
const int ccw_pin = 4;
const int stop_pin = 7;
const int analog_in_pin = A0;

void setup() {
  // set the digital pin as output and analog pin as input:
  pinMode(pwm_pin, OUTPUT);
  pinMode(enable_pin, OUTPUT);
  pinMode(ccw_pin, OUTPUT);
  pinMode(stop_pin, OUTPUT);
  pinMode(analog_in_pin, INPUT);
  // Initialize serial communication at 9600bps (used to track velocity)
  Serial.begin(9600);
}

void loop() {
  digitalWrite(stop_pin, LOW);
  digitalWrite(enable_pin, HIGH);

  // Test data
  digitalWrite(ccw_pin, HIGH);
  analogWrite(pwm_pin, 0); // Corresponds to about 200rpm
  delay(200);
}
