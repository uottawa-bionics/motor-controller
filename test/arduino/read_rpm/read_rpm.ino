// Read RPM
// Joshua O'Reilly
// 2019/04/26
// This program sends out an analog signal and reads the rpm from the motor
// See attached images for wiring

// For this setup:
// Digital Output 1: PWM set value
//        10%: 0rpm
//        90%: 1500rpm
// Digital Output 2: Enable or disable
// Digital Output 3: Direction (high is CCW, low is CW)
// Digital Output 4: Stop
// Analog Input 0: Rotational Velocity 
const int pwm_pin = 5;
const int enable_pin = 2;
const int ccw_pin = 4;
const int stop_pin = 7;
const int analog_in_pin = A0;

int sensor_value_rpm = 0;

float average = 0;
int number_of_readings = 0;

void setup() {
  // set the digital pin as output and analog pin as input:
  pinMode(pwm_pin, OUTPUT);
  pinMode(enable_pin, OUTPUT);
  pinMode(ccw_pin, OUTPUT);
  pinMode(stop_pin, OUTPUT);
  pinMode(analog_in_pin, INPUT);
  // Initialize serial communication at 9600bps (used to track velocity)
  Serial.begin(9600);
}

void loop() {
  digitalWrite(stop_pin, LOW);
  digitalWrite(enable_pin, HIGH);

  // Test data
  digitalWrite(ccw_pin, HIGH);
  analogWrite(pwm_pin, 25); // Corresponds to about 200rpm
  sensor_value_rpm = 1.2 * get_current_rpm();
  average = ((average * number_of_readings) + sensor_value_rpm) / (number_of_readings + 1);
  number_of_readings += 1;
  Serial.print("Average: ");
  Serial.println(average);
  Serial.print("Current: ");
  Serial.println(sensor_value_rpm);
  delay(200);
}

/*
 * Motor specs:
 * 1610 max RPM
 * 10% PWM: 161rpm
 * 90% PWM: 1449 rpm
 * 100% PWM: 1610rpm
 */

/*
 * analogRead maps 0-5V between 0-1023
 * Since Arduino accepts 5V in, we need
 * 4/5 of max value be equal to 1600rpm (since we'll never read over 4/5 of 5V)
 * (This all assumes that the mappings made by ESCON and Arduino are linear)
 */
int get_current_rpm(){
  int sensor_value_raw = analogRead(analog_in_pin);
  // 818 is 4/5 of 1023
  int sensor_value_rpm = map(sensor_value_raw, 0, 818, -1610, 1610);
  return sensor_value_rpm;
}
