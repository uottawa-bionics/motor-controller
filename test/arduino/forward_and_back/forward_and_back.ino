// Forward and back
// Joshua O'Reilly
// 2019/04/26
// This program simply alternates turning the motor clockwise and counter-clockwise
// See the attached images for wiring

// For this setup:
// Digital Output 1: PWM set value
//        10%: 0rpm
//        90%: 1500rpm
// Digital Output 2: Enable or disable
// Digital Output 3: Direction (high is CCW, low is CW)
// Digital Output 4: Stop

const int pwm_pin = 5;
const int enable_pin = 2;
const int ccw_pin = 4;
const int stop_pin = 7;

void setup() {
  // set the digital pin as output:
  pinMode(pwm_pin, OUTPUT);
  pinMode(enable_pin, OUTPUT);
  pinMode(ccw_pin, OUTPUT);
  pinMode(stop_pin, OUTPUT);
}

void loop() {
  digitalWrite(enable_pin, HIGH);
  digitalWrite(stop_pin, LOW);

  // Turn Counter-ClockWise
  digitalWrite(ccw_pin, HIGH);
  for(int i=20; i<50;i++){
    analogWrite(pwm_pin, i);
    delay(100);
  }
  for(int i=50; i<20;i--){
    analogWrite(pwm_pin, i);
    delay(100);
  }

  // Turn off motor for two seconds
  digitalWrite(enable_pin, LOW);
  delay(2000);
  digitalWrite(enable_pin, HIGH);
  
  // Turn ClockWise
  digitalWrite(ccw_pin, LOW);
  for(int i=20; i<50;i++){
    analogWrite(pwm_pin, i);
    delay(100);
  }
  for(int i=50; i<20;i--){
    analogWrite(pwm_pin, i);
    delay(100);
  }

  // Turn off the motor for two seconds, then repeat
  digitalWrite(enable_pin, LOW);
  delay(2000);
}
