# Motor Controller

This repo contains code to control the EC90 Maxon Motors, primarily via ESCON 50/4 controllers.

## Test Files

For test files, navigate to /test. So far, includes the following test files to get you started. All scripts should also include basic images to help get you started:

- **forward_and_back**: Arduino script that alternates moving the motor forward and back at low speeds
- **read_rpm**: Arduino script that sends an PWM signal to the motor, and reads the RPM given by the encoder
